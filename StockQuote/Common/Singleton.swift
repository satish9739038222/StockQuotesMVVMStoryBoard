//
//  Singleton.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class Singleton: NSObject {
    static let sharedInstance = Singleton()
    var arrayToStoreSymbolQuotes = [[String: Any]]()
    var arraySymbol : [String] = ["IBM","AAPL","GOOGL","MSFT","INTC","ORCL","IBN","INFY","C","UBS","TTM","DB","MCD","PAG","PNC","NEXA"]
    var didTapOnRefresh = false
    override init(){
    
    }
    
    func threeDecimalFormatter(number : NSNumber) -> String {
        let threeDecimalFormatter = NumberFormatter()
        threeDecimalFormatter.numberStyle = NumberFormatter.Style.decimal
        threeDecimalFormatter.maximumFractionDigits = 3
        threeDecimalFormatter.roundingMode = NumberFormatter.RoundingMode.up
        return threeDecimalFormatter.string(from: number)!
    }
}

