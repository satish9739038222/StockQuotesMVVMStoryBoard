//
//  CustomHeader.swift
//  StockQuote
//
//  Created by Satish Kumar on 5/5/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit
protocol CustomHeaderCellDelegate{
    
    func didClickedOnSymbolButtonDelegate()
    func didClickedOnPriceButtonDelegate()

}

class CustomHeader: UITableViewCell {

    var delegate : CustomHeaderCellDelegate?
    
    
    @IBAction func didClickedOnSymbolButtonDelegate(sender: AnyObject) {
        //let indexPath = self.superTableView().indexPath(for: self)
        //if let _ = indexPath {
            self.delegate!.didClickedOnSymbolButtonDelegate()
       // }
    }
    
    @IBAction func didClickedOnPriceButtonDelegate(sender: AnyObject) {
        //let indexPath = self.superTableView().indexPath(for: self)
        //if let _ = indexPath {
            self.delegate!.didClickedOnPriceButtonDelegate()
        //}
    }
    
    func superTableView()->UITableView{
        if let superview = self.superview?.superview {
            if superview is UITableView {
                return superview as! UITableView
            }
        }
        return UITableView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
