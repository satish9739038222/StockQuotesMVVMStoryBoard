//
//  HomePageViewController.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//
//https://eodhistoricaldata.com/api/eod/AAPL.US?from=2017-01-05&to=2017-03-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d&fmt=json
//https://eodhistoricaldata.com/api/eod/INTC.US?from=2017-01-05&to=2017-03-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d&fmt=json
//https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=AAPL&interval=1min&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=MSFT,FB,AAPL,GOOGL,INTC,IBM,ORCL&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=FB,AAPL,ORCL&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=IBM&apikey=9P8WY4UDZRXZZWFC
//https://api.iextrading.com/1.0/stock/IBM/quote

//https://api.iextrading.com/1.0/stock/IBM/quote
//https://api.iextrading.com/1.0/stock/AAPL/quote
//https://api.iextrading.com/1.0/stock/GOOGL/quote
//https://api.iextrading.com/1.0/stock/MSFT/quote
//https://api.iextrading.com/1.0/stock/INTC/quote
//https://api.iextrading.com/1.0/stock/ORCL/quote

import UIKit

class HomePageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CustomHeaderCellDelegate {
    let symbolViewModel = SymbolViewModel()
    var isPriceAscending = false
    var isSymbolAscending = false
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?

    @IBOutlet weak var homePageTableView: UITableView!
    //weak var movableImageView: UIImageView! = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController?.navigationBar.barTintColor  = UIColor(red: 0.141, green: 0.569, blue: 0.788, alpha: 1.0)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem?.setTitleTextAttributes(
            [NSAttributedStringKey.foregroundColor: UIColor.blue], for: [])
        
        self.homePageTableView.rowHeight = UITableViewAutomaticDimension
        self.homePageTableView.estimatedRowHeight = 72
        
        let movableImageView = UIImageView()
        movableImageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        movableImageView.backgroundColor = UIColor.red
        
        self.homePageTableView.window?.addSubview(movableImageView)
        
        JustHUD.shared.showInView(view: view, withHeader: "Loading", andFooter: "Please wait...")
        self.navigationController?.navigationBar.isHidden = true
        
        symbolViewModel.getSymbols {
            self.homePageTableView.reloadData()
            JustHUD.shared.hide()
            self.navigationController?.navigationBar.isHidden = false
        }
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return symbolViewModel.arrayToStoreSymbolInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = self.homePageTableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell

            cell.symbolName.text = symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].symbol
            cell.symbolNameDesc.text = symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].companyName
            cell.currentTrade.text = Singleton.sharedInstance.threeDecimalFormatter(number: symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].latestPrice) //(symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].latestPrice).stringValue
            
             cell.deltaInPercentage.text = Singleton.sharedInstance.threeDecimalFormatter(number: symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].changePercent)
            
            if ((symbolViewModel.arrayToStoreSymbolInfo[indexPath.item].changePercent).stringValue.hasPrefix("-")){
                cell.deltaInPercentage.textColor = UIColor.red
            }else{
                cell.deltaInPercentage.textColor = UIColor.green
            }
        
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("CustomHeader", owner: self, options: nil)?.first as! CustomHeader
        headerView.delegate = self
        return headerView
    }
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 {
            return 35
        } else {
            return 71
        }
    } */
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    @IBAction func didTapOnRefresh(_ sender: Any) {
        Singleton.sharedInstance.didTapOnRefresh = true
        symbolViewModel.getSymbols {
            self.homePageTableView.reloadData()
        }
    }
    
    func didClickedOnSymbolButtonDelegate(){
        if(isSymbolAscending){
            symbolViewModel.arrayToStoreSymbolInfo.sort() { $0.symbol < $1.symbol }
            isSymbolAscending = false
        }else{
            symbolViewModel.arrayToStoreSymbolInfo.sort() { $0.symbol > $1.symbol }
            isSymbolAscending = true
        }
        self.homePageTableView.reloadData()
    }
    
    func didClickedOnPriceButtonDelegate(){
        if(isPriceAscending){
            symbolViewModel.arrayToStoreSymbolInfo.sort() { $0.latestPrice.doubleValue < $1.latestPrice.doubleValue }
            isPriceAscending = false
        }else{
            symbolViewModel.arrayToStoreSymbolInfo.sort() { $0.latestPrice.doubleValue > $1.latestPrice.doubleValue }
            isPriceAscending = true
        }
        self.homePageTableView.reloadData()
    }
    
    @IBAction func didTapOnComposeButton(_ sender: Any) {

       
    }
    
  
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: segue.destination)

        segue.destination.modalPresentationStyle = .custom
        segue.destination.transitioningDelegate = self.halfModalTransitioningDelegate
    }
    

}



class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var symbolName: UILabel!
    @IBOutlet weak var symbolNameDesc: UILabel!
    @IBOutlet weak var currentTrade: UILabel!
    @IBOutlet weak var deltaInPercentage: UILabel!
    
}
